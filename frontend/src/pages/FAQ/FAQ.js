import React, {Component} from "react";
import "./FAQ.css";

class FAQ extends Component{
    render(){
        return (
            <div class="faq-container">
                <h1>Q: Get current beatmap doesn't work.</h1>
                <p>Make sure you set the right path to your Songs folder in the Preferences window.</p>
                <h1>Q: How do I use any tool?</h1>
                <p>Most tools work by changing the .osu file of your beatmap. Make sure you set the correct path to your beatmap (visible on the 
                    blue bar on the bottom) using the options in the File menu. Then run the tool by clicking the play button on the bottom left. 
                    After running the tool always reload the editor (Ctrl+L) so the changes to the .osu show up in the editor.
                    You can also look in the documentation to find some tutorials.
                </p>
                <h1>Q: A tool broke my map forever. What do I do?</h1>
                <p>By default, Mapping Tools always make a backup of any map that gets edited. 
                    You can find these backups by clicking Options -> Open backup folder.</p>
                <h1>Q: I have a question not on this list or I found a bug.</h1>
                <p>Go to the mapping-tools-techsupport channel in this <a href="https://discord.gg/JhP964H">Discord</a> or 
                            ask me directly on Discord (OliBomby#3573) or make a new issue on the <a href="https://github.com/OliBomby/Mapping_Tools">
                            GitHub</a>.</p>
            </div>
        );
    }
}

export default FAQ;